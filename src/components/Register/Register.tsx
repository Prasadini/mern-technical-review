import React, { useState } from 'react'
import { Grid, Paper, Avatar, TextField, Button, Typography, FormControlLabel, Checkbox } from "@mui/material";
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import type { AlertColor } from '@mui/material/Alert';

interface IFormInput {
  firstName: string,
  lastName: string,
  email: string,
  phoneNumber: string,
  password: string
}

const Register = () => {

  const paperStyle = { padding: 20, height: 'auto', width: 560, margin: "20px auto" }
  const avatarStyle = { backgroundColor: '#1bbd7e' }
  const btnstyle = { margin: '8px 0' }
  const formControlStyle = { margin: '8px' }
  const error = { color: "red" }

  // data

  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    password: ""
  })
  const [message, setMessage] = React.useState({
    severity: "",
    message:""
  });
  //alert
  const [open, setOpen] = React.useState(false);

  const handleChange = (e: { target: { name: any; value: any; }; }) => {
    const { name, value } = e.target
    setUser({
      ...user,//spread operator 
      [name]: value
    })

  }


  const onSubmitRegister: SubmitHandler<IFormInput> = data => {

    if (data.firstName && data.email && data.password && data.lastName
      && data.phoneNumber) {
      axios.post("http://localhost:8000/user/create", data)
        .then((res) => {
          reset()
          console.log(res)
          setMessage({
            severity:"success",
            message:res.data.message
          });
         
          setOpen(true);

        }).catch((err) => {
          setMessage({
            severity:"error",
            message:err.response.data.message
          });
          
          setOpen(true);

        })
    }
    else {
      alert("invalid input")
    };
  }

  const { control, register, formState: { errors }, handleSubmit,reset  } = useForm<IFormInput>();

  return (
    <Grid>
      <Paper elevation={10} style={paperStyle}>
        {open ? <Alert
          variant="filled" severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {message.message}
        </Alert> : ""}
        <Grid container>
          {/* <Avatar style={avatarStyle}><LockOutlinedIcon/></Avatar> */}
          <h2>Register</h2>
        </Grid>

        <form onSubmit={handleSubmit(onSubmitRegister)}>
        <Controller
            control={control}
            {...register("firstName", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} label='First Name' placeholder='Enter first name'
            variant="outlined"
            fullWidth
            style={formControlStyle}
          />}
          />
          <Typography style={error}>{errors.firstName && "First name is required"}</Typography>

          <Controller
            control={control}
            {...register("lastName", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} label='Last Name' placeholder='Enter last name' type='text' variant="outlined" fullWidth
            style={formControlStyle} />}
          />
          <Typography style={error}>{errors.lastName && "Last name is required"}</Typography>

          <Controller
            control={control}
            {...register("email", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} label='Email' placeholder='Enter email' variant="outlined" type='email' fullWidth style={formControlStyle} />}
          />
          <Typography style={error}>{errors.email && "Email is required"}</Typography>

          <Controller
            control={control}
            {...register("phoneNumber", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} label='Phone Number' placeholder='Enter phone number' variant="outlined" fullWidth style={formControlStyle} />}
          />
          <Typography style={error}>{errors.phoneNumber && "Phone Number is required"}</Typography>

          <Controller
            control={control}
            {...register("password", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} label='Password' type="password" placeholder='Enter password' variant="outlined" fullWidth style={formControlStyle} />}
          />
          <Typography style={error}>{errors.password && "Password is required"}</Typography>                  

          <Button type='submit' color='primary' variant="contained" style={btnstyle} fullWidth>Register</Button>
        </form>

        <Typography > Do you have an account ?
          <Link to="/login" >
            Sign In
          </Link>
        </Typography>
      </Paper>
    </Grid>
  )

}
export default Register