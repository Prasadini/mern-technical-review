import React, { useState } from 'react'
import { Grid, Paper, Avatar, TextField, Button, Typography, FormControlLabel, Checkbox } from "@mui/material"
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import { red } from '@mui/material/colors';

interface IFormInput {
  email: string;
  password: string;
}

const Login = () => {

  const paperStyle = { padding: 20, height: '70vh', width: 560, margin: "20px auto" }
  const avatarStyle = { backgroundColor: '#1bbd7e' }
  const btnstyle = { margin: '8px 0' }
  const formControlStyle = { margin: '8px 0' }
  const error = { color: "red" }

  let navigate = useNavigate();

  const onSubmit: SubmitHandler<IFormInput> = data => {
    if (data.email && data.password) {
      axios.post("http://localhost:8000/login", data)
        .then((res) => {
          if (res.status == 200) {
            const token = res.data.token;
            const user = JSON.stringify(res.data.user);
            localStorage.setItem('token', token);
            localStorage.setItem('user', user);
            setLogUser({
              ...logUser,//spread operator 
              email: "",
              password: ""
            })
            goHome();
          }

        }).catch((err) => {
          setMessage(err.response.data.message);

          setOpen(true);

        })
    }
    else {
      alert("invalid input");
    };
  };
  //alert
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState("");

  // data
  const [logUser, setLogUser] = useState({
    email: "",
    password: ""
  })
  const goHome = () => {
    navigate("/");
  };
  
  const { control, register, formState: { errors }, handleSubmit } = useForm<IFormInput>();

  return (
    <Grid>
      <Paper elevation={10} style={paperStyle}>
        {open ? <Alert
          variant="filled" severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {message}
        </Alert> : ""}

        <Grid container>
          <h2>Sign In</h2>
        </Grid>

        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            control={control}
            {...register("email", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} 
            label='Email' placeholder='Enter email' 
            variant="outlined" fullWidth
            style={formControlStyle} />}
          />
          <Typography style={error}>{errors.email && "Email is required"}</Typography>
          <Controller
            control={control}
            {...register("password", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} label='Password' placeholder='Enter password' fullWidth
              type='password' variant="outlined"
              style={formControlStyle} />}
          />
          <Typography style={error}>{errors.password && "Password is required"}
          </Typography>

          <Button type='submit' color='primary' variant="contained" style={btnstyle} fullWidth>Sign in</Button>
        </form>

        <Typography > Do you have an account ?
          <Link to="/register" >
            Sign Up
          </Link>
        </Typography>
      </Paper>
    </Grid>
  )
}

export default Login