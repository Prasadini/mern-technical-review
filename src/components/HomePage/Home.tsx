import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';




const Home = () => {

    const [user, setUser] = useState({
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: "",
    });
    const getUserFromLocalStorage = () => {
        const userJson = localStorage.getItem("user");
        const userInfo = userJson !== null ? JSON.parse(userJson) : {};
        setUser({
            ...user,//spread operator 
            firstName: userInfo.firstName,
            lastName: userInfo.lastName,
            email: userInfo.email,
            phoneNumber: userInfo.phoneNumber
        })

    }
    useEffect(() => {
        getUserFromLocalStorage();
    }, []);

    return (
        <Box sx={{ minWidth: 275 }}>
            <Card variant="outlined">
                <React.Fragment>
               
                    <CardContent>
                    <Typography sx={{ mb: 1.5 }} variant="h5" gutterBottom>
                               User Deatils
                            </Typography>
                        {Object.values(user).map((value,index) => (
                            <Typography sx={{ mb: 1.5 }} color="text.secondary" key={index}>
                                 {value}
                            </Typography>
                        ))}

                    </CardContent>

                </React.Fragment></Card>
        </Box>
    );
}

export default Home;