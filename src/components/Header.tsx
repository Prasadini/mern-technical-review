import React, { useState, useEffect } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Link,useNavigate } from "react-router-dom";


const btnstyle = { textDecoration: "none", color: "white" }

export default function Header() {
  const [isAuth, setUser] = React.useState(false);
  
  const getUserFromLocalStorage = () => {
    const userInfo = localStorage.getItem("token")? true:false;
    setUser(userInfo);

  }
  useEffect(() => {
    getUserFromLocalStorage();
  }, []);

  const handleLogout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    getUserFromLocalStorage();
    goLogin()
  }
  let navigate = useNavigate();
  const goLogin = () => {
    navigate("/login");
  };
  const goResetPassword = () => {
    navigate("/reset-password");
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            {/* <MenuIcon /> */}
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            MERN-STACK
          </Typography>
          {/* <Button color="inherit">Login</Button> */}
          {isAuth ? (
            <Button style={btnstyle} onClick={handleLogout}>logout</Button>
            
          ) : (
            <Link to="register" style={btnstyle}> <Button color="inherit">Register</Button></Link>
          )}
          {isAuth? (<Button style={btnstyle} onClick={goResetPassword}>Reset Password</Button>) :""}

        </Toolbar>
      </AppBar>
    </Box>
  );
}