import React,{useState} from 'react'
import { Grid, Paper, Avatar, TextField, Button, Typography, FormControlLabel, Checkbox } from "@mui/material";
import { Link } from 'react-router-dom';
import axios from 'axios';
import { authAxios } from '../../setAuthToken';
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

interface IFormInput {
  email: string;
  password: string;
}

const paperStyle = { padding: 20, height: 'auto', width: 560, margin: "20px auto" }
const avatarStyle = { backgroundColor: '#1bbd7e' }
const btnstyle = { margin: '8px 0' }
const formControlStyle = {margin: '8px' }
const error = { color: "red" }

const ResetPassword = () => {

  const [user, setUser] = useState({
    email: "",
    password: ""
  })
  
  const handleChange = (e: { target: { name: any; value: any; }; }) => {
    const { name, value } = e.target
    setUser({
      ...user,//spread operator 
      [name]: value
    })
  }
  const [message, setMessage] = React.useState({
    severity: "",
    message:""
  });
  //alert
  const [open, setOpen] = React.useState(false);

  const onSubmitRegister: SubmitHandler<IFormInput> = data => {
   
    if (data.email && data.password) {
      authAxios.post("/reset-password", data)
        .then((res) => {
          
          reset()
          console.log(res)
          setMessage({
            severity:"success",
            message:res.data.message
          });
         
          setOpen(true);
        }).catch((err) => {
          setMessage({
            severity:"error",
            message:err.response.data.message
          });
          
          setOpen(true);

        })
    }
    else {
      alert("invalid input")
    };
  }
  const { control, register, formState: { errors }, handleSubmit,reset } = useForm<IFormInput>();

  return (
    <Grid>
      <Paper elevation={10} style={paperStyle}>
      {open ? <Alert
          variant="filled" severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {message.message}
        </Alert> : ""}
        <Grid container>
          {/* <Avatar style={avatarStyle}><LockOutlinedIcon/></Avatar> */}
          <h2>Reset Password</h2>
        </Grid>
        <form onSubmit={handleSubmit(onSubmitRegister)}>
        <Controller
            control={control}
            {...register("email", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} name="email" label='Email' placeholder='Enter email' variant="outlined" fullWidth style={formControlStyle} />}
          />
          <Typography style={error}>{errors.email && "Email is required"}</Typography>
        
          <Controller
            control={control}
            {...register("password", { required: true })}
            defaultValue=""
            render={({ field }) => <TextField {...field} name="password" label='New Password' placeholder='Enter password' type='password' variant="outlined" fullWidth style={formControlStyle} />}
          />
          <Typography style={error}>{errors.password && "Password is required"}</Typography>

       
        
        <Button type='submit' color='primary' variant="contained" style={btnstyle} fullWidth
        >Reset Password</Button>
        </form>
      </Paper>
    </Grid>
  )
}

export default ResetPassword