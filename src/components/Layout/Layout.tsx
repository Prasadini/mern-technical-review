import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Header from "../../components/Header";
import Footer from "../../components/Footer";


const Layout: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box sx={{ bgcolor: '#f1f1f2' }}>
          <Header />
          {children}
          <Footer></Footer>
        </Box>
      </Container>
    </React.Fragment>
  )
}

export default Layout