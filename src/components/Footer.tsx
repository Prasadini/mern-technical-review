import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';


  
  export default function Footer() {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
        © 2021 Copyright:
        <a className='text-reset fw-bold' href='https://mdbootstrap.com/'>
          MDBootstrap.com
        </a>
      </div>
      </Box>
    );
  }

