import axios from 'axios';

const apiUrl = "http://localhost:8000";
const accessToken = localStorage.getItem("token");
console.log(accessToken);

const authAxios = axios.create({
    baseURL: apiUrl,
    headers:{
        Authorization:`Bearer ${accessToken}`
    }
})

 export {authAxios}
// export const setAuthToken = (token:any) => {
//    if (token) {

//        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
//    }
//    else
//        delete axios.defaults.headers.common["Authorization"];
// }