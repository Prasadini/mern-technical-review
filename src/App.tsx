import Login from './components/LoginPage/Login';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import Layout from './components/Layout/Layout';
import * as React from 'react';
import Button from '@mui/material/Button';
import {
  BrowserRouter,
  Routes, //replaces "Switch" used till v5
  Route,
  Link
} from "react-router-dom";
import Register from './components/Register/Register';
import ResetPassword from './components/ResetPassword/ResetPassword';
import Home from './components/HomePage/Home';
import PrivateRoutes from './components/PrivateRoute';
import ValidationTest from './components/ValidationTest';

function App() {

  return (
    <div className="App">
      {/* base router */}
           
        <BrowserRouter>
        <Layout>
        <Routes>
            <Route path="/login" element={<Login />} />
            <Route element={<PrivateRoutes />}>
            <Route path="/reset-password" element={<ResetPassword />} />
            <Route path="/" element={<Home />} />
            </Route>
            <Route path="/register" element={<Register />} />
            <Route path="/validate" element={<ValidationTest />} />
            
          </Routes>
        </Layout>
          
        </BrowserRouter>
      

    </div>
  );
}

export default App;
